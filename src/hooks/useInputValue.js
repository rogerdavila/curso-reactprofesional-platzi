import { useState } from 'react'

export const useInputValue = initialValue => {
  const [value, setValue] = useState(initialValue)
  const onChange = e => e ? setValue(e.target.value) : setValue(initialValue)
  return { value, onChange }
}
