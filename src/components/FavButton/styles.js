import styled from 'styled-components'

export const Button = styled.button`
    padding-top: 8px;
    background: none;
    border: 0;
    display: flex;
    align-items: center;
    & svg {
        margin-right: 4px;
    }    
`
