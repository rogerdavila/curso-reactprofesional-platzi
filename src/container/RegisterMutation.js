import React from 'react'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

const REGISTER = gql`
    mutation signup($input: UserCredentials!) {
        signup(input: $input)
    }
`

export const RegisterMutation = ({ children }) => (
  <Mutation mutation={REGISTER}>
    {children}
  </Mutation>
)
